---
layout: SpecialPage
---
# About Us

<!--[sunlab-team](images/avatar/aboutus.jpg "Sunlab team")-->

![sunlab-team](images/avatar/aboutus.jpg "Sunlab team")

## WebEx Office Hour

We will host office hour(40 mins per person) through WebEx or bluejean **Weekly**. Email him/her if you have trouble to join the meeting. See below table for detailed schedule
For on-campus students, we will hold office hours right after lectures. Please find the corresponding TA regular duties on Piazza, if you have any question, email him/her when needed.

| Photo| Name|Time (EST)             | Location or Web Link |
| :-------------: | :-------------: | ---------------- | --------------------------------------------------------------------------------------|
|![minipic](images/avatar/Jimeng.png)   |  Jimeng Sun, instructor jsun<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;cc.gatech.edu     |      |       Request by email         |
|![minipic](images/avatar/jeff.jpg)   |  Jeffrey Valdez, instructor valdez@<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;cc.gatech.edu     |      |       Request by email         |
|![minipic](images/avatar/MingLiu.jpg) | Ming Liu , PhD,  TA mliu302<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| Tues 10AM |<https://gatech.webex.com/meet/mliu302>
|![minipic](images/avatar/SungtaeAn.jpg) | Sungtae An, Sunlab PhD,  TA  stan84<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| Mon 3PM | <https://gatech.webex.com/meet/san37>
|![minipic](images/avatar/park32.jpg) | Su Young Park, MS, TA spark638<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| Wed 2PM | <https://gatech.webex.com/meet/spark638>
|![minipic](images/avatar/siddharth.png) | Siddharth, PhD,  TA sbiswal7<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| Thurs 4:30PM | <https://gatech.webex.com/join/sbiswal7>
|![minipic](images/avatar/Ari.jpg) | Ardavan (Ari) Afshar, PhD, TA aafshar8<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| Wed 12PM | <https://gatech.webex.com/meet/aafshar8>
|![minipic](images/avatar/Irfan.jpg) | Irfan Al-Huissani, PhD, TA iah3<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| Mon 10AM | <https://gatech.webex.com/meet/iah3>
|![minipic](images/avatar/wendi.jpg) | Wendi Ren, MS, TA wren44<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| Tues 1:30PM | <https://gatech.webex.com/meet/wren44>
|![minipic](images/avatar/tianfan1.jpg) | Tianfan Fu, PhD, TA tfu42<span style="display:none">hello</span>&nbsp;AT<span style="display:none">world</span>&nbsp;gatech.edu| Fri 4PM | <https://gatech.webex.com/meet/tfu42>



