---
layout: SpecialPage
sidebarDepth: 1
---
# CS498 Deep Learning for Healthcare

<div class="main-explain-area jumbotron">

This website covers information for [University of Illinois Urbana-Champaign](//cs.illinois.edu/)'s Fall 2020 course **CS498 Deep Learning for Healthcare**.  All students may refer to this site for most up to date content.

+ Instructor: [Prof. Jimeng Sun](//sunlab.org)
+ Discussion: [CS498 Fall 2020 Piazza](https://piazza.com/class/kbm74itomdi569)
+ Location: Online only

</div>

## Course Description

Welcome to Deep Learning for Healthcare. This course covers deep learning (DL) methods, healthcare data and applications using DL methods. 
The courses include activities such as video lectures, self guided programming labs, homework assignments (both written and programming), and a large project. 

The first phase of the course will include video lectures on different DL and health applications topics, self-guided labs and multiple homework assignments. 
In this phase, you will build up your knowledge and experience in developing practical deep learning models on healthcare data. 
The second phase of the course will be a large project that can lead to a technical report and functioning demo of the deep learning models
for addressing some specific healthcare problems. We expect the best projects can potentially lead to scientific publications. 

## Course Objectives
You are expected to learn deep learning models such as deep neural networks, convolutional neural networks, recurrent neural networks, autoencoder, attention models, 
graph neural networks and deep generative learning. You will also get a chance to learn different healthcare applications using DL methods such as clinical predictive models, 
computational phenotyping, patient risk stratification, treatment recommendation, clinical natural language processing, and medical imaging analysis. Besides learning DL algorithms, 
the course will focus on hands-on experiences for data scientists and machine learning engineers to implement various practical healthcare models on diverse medical data. 
You will learn popular deep learning frameworks like pytorch, and data science software like jupyter notebook. 


## Prerequisites
**Basic machine learning** will be helpful but not strictly required. 
You should have **good programming skills** in python and good understanding in **linear algebra** and calculus. 
You should also have **sufficient system knowledge** such as using linux, setting up programming environments on the cloud. 

## Textbook and Readings
A textbook is being developed by the instructor. 
The corresponding chapters will be provided to the students. 
Also relevant research papers will be provided to you. You are also encouraged to conduct your own literature review especially during the project phase.

## Elements of This Course
The following elements are in this course: 

- **Video lectures**: They are recorded by the instructor to cover technical topics about deep learning and related healthcare applications. 
- **Labs**: They are self-guided study materials that provide the step by step instructions of the programming tasks related to building deep learning models and manipulating healthcare data. 
- **Homework assignments**: They contain both the written parts and the programming parts. The written parts involve mathematical derivation and short answer questions. The programming parts involve completing data processing scripts, building and evaluating DL models. 
- **Project**: a large group project at the second phase of the course:

1. Each project team consists of 1 to 4 students. 
2. Project topics are provided in the project overview document in the course. With permission from the instructor, a different project can be pursued as long as they are about deep learning for healthcare problems. The project should be exclusively done for this course. The students should NOT use this project from or for other courses or for other purposes (such as part of your daily job). 
3. Project proposal: a short clearly written description of your project that provides the overview of your project. It should clearly describe the data, the task and the evaluation metric and the key related works. 
4. Project draft: a complete write-up about the project including some preliminary experiment results. 
5. Project presentation: a 5-min video presentation that summarizes the key results from your project. It should describe the task, the data, the method and the result. It should be within 5-min. 
6. Project final report: a well written report that summarizes all the findings from your project. It should look like a publishable technical report. 


## Grading scheme

- Homework assignments: 50% (10% each)
- Weekly reflection: 15% 
- Project proposal 3%
- Project draft 7%
- Project final presentation 10%
- Project report 15%


## Student Code and Policies 
A student at the University of Illinois at the Urbana‑Champaign campus is a member of a University community of which all members have at least the rights and responsibilities common to all citizens, free from institutional censorship; affiliation with the University as a student does not diminish the rights or responsibilities held by a student or any other community member as a citizen of larger communities of the state, the nation, and the world. See the University of Illinois Student Code for more information. Academic Integrity All students are expected to abide by the campus regulations on academic integrity found in the Student Code of Conduct. These standards will be enforced and infractions of these rules will not be tolerated in this course. Sharing, copying, or providing any part of a homework solution or code is an infraction of the University’s rules on academic integrity. We will be actively looking for violations of this policy in homework and project submissions. Any violation will be punished as severely as possible with sanctions and penalties typically ranging from a failing grade on this assignment up to a failing grade in the course, including a letter of the offending infraction kept in the student's permanent university record. Again, a good rule of thumb: Keep every typed word and piece of code your own. If you think you are operating in a gray area, you probably are. If you would like clarification on specifics, please contact the course staff. 

## Disability Accommodations 
Students with learning, physical, or other disabilities requiring assistance should contact the instructor as soon as possible. If you’re unsure if this applies to you or think it may, please contact the instructor and Disability Resources and Educational Services (DRES) as soon as possible. You can contact DRES at 1207 S. Oak Street, Champaign, via phone at (217) 333-1970, or via email at disability@illinois.edu. 

## Late Policy 
Each student is allowed 2 days of late submission in total to be used for HOMEWORK only. You can split the 2 days grace period across two different homework (20 hours will be counted as 1 day and 30 hours counted as 2 days). Once you have used up your late days, late assignments will be penalized at a rate of 10% per day (10% of the total points not 10% of your score. If your score is 60 but you are late for 1 day, your score will become 50). Assignments more than 5 days late will not be accepted. Also, you can't apply the late days toward the final project or Kaggle competition. We rarely consider other homework extension requests due to the volume of the class, so please schedule your arrangements well in advance.

