---
layout: SpecialPage
---
# Project

## Project format

We allow individual or group projects (up to 4 students per group, grading rubrics is same for team project and individual project). You are responsible to form groups on Piazza/Email each other. We will provide some project ideas but you can also propose your own projects with the instructor's approval. The successful resulting project can potentially lead to research publication at health informatic venues such as [AMIA](https://www.amia.org/amia2017), [JAMIA](http://jamia.oxfordjournals.org/) and [JBI](https://www.journals.elsevier.com/journal-of-biomedical-informatics/).

<!--See **T-Square resource > Project related > project guide.pdf** for detailed requirements.-->

## Important Dates

|Due day     |  Due day|
|---------|:------------------:|
|Feb 16   |Project group formation |
|Mar 1    |Project proposal |
|Apr 12  |Project draft|
|Apr 26 |Final Project (code+presentation+final paper) |

## Final project

Students should submit source code, presentation slides, and final paper pdf to Canvas. 

## Canvas

Students will use Canvas for submission of project proposal, draft (and final paper).

<!--
## Peer Review (optional)

Please use below framework for peer review

```
[Summary]: What is the paper about?

[Strength]
S1
S2
S3
[Weakness]
W1
W2
W3
[Intellectual merit] (what is the novelty of the approach?)

[Broader impact] (how important is the proposed problem?)

[Evaluation] (how comprehensive is their evaluation? how good are their results?) 

```
-->

<!--
# AWS
For homework, lab and project, you may consider using Amazon Web Service(AWS) as the platform. Here is a general instruction about how to setup AWS account.

## Create account
We strongly suggest you to setup a new AWS account using the GT email for this course, or re-use an existing account for this course alone. If you don't have an account yet, you can create an AWS Account by:

1. Sign up for an AWS account by clicking on [this link](https://portal.aws.amazon.com/gp/aws/developer/registration/index.html)
2. Enter your email, billing address and credit card information etc (required to verify your valid identity - your card will not be charged unless your usage exceeds the free usage tiers)
3. You can login to your [AWS Management Console](https://console.aws.amazon.com/console/home) to verify successful sign up.

## Consolidated billing [optional]
With a valid AWS account, we can setup consolidated bill. Consolidated bill will bill your AWS cost to us. But doing so will also merge all your existing credits to use if you have. If you don't want to let that happen you can skip this step or do it later.

1. Submit account information to us via this [google form](https://goo.gl/forms/2AEpxsdFZy2UNCXd2).
2. Wait for email invitation to join consolidated bill.
3. Accept consolidated billing invitation.

We will generate AWS cost report regularly, please refer to the report to see your usage compared to your classmates.

## <span style="color:red">Important</span>
<p style="color:red;font-weight:bold">
No matter you are using your own credit or consolidated billing, check AWS usage frequently to make sure it doens't exceed budget. If we find your AWS cost is unreasonably high(i.e. much higher than median level), we will stop consolidated billing.
</p>
-->
