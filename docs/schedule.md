---
layout: SpecialPage
---
[[toc]]


## Schedule
|Week #|Dates    |Topics		                             |Video lessons                                       |Lab                                   |Deliverable Due                                                             |
|:-----|:--------|:------------------------------------------|:---------------------------------------------------|:-------------------------------------|:---------------------------------------------------------------------------|
|1     |		 |Introduction					             |1. Intro to DL for healthcare                       | Pandas, scikit 	           			 |                                                                            |
|2     |		 |Machine learning 		                     |2. Machine learning basics                          |	Predictive modeling				     |                                                                            |
|3     |		 |Health data								 |3. Health data                      				  |	Health data processing				 |   HW1 Due                                                              |
|4     |		 |Deep Neural Networks (DNN)				 |4. Deep Neural Networks (DNN)		                  |   Pytorch, 	   DNN                   |                                                                            |
|5     |		 |Embedding (word2vec, t-SNE, etc)	    	 |5. Embedding						  				  | Word2vec, t-SNE, UMAP			     |   HW2 Due                                                                 |
|6     |		 |Convolutional Neural Networks (CNN)        |6. Convolutional Neural Network (CNN)        		  | CNN                                  |                                                                            |
|7     |		 |Recurrent Neural Networks (RNN)	         |7. Recurrent Neural Networks (RNN)	              | RNN                                  |  Project proposal due                                                              |
|8     |		 |Autoencoders								 |8. Autoencoders                                     | Autoencoders						 |  HW3 Due																	   | 
|9     |		 |Attention Models						     |9. Attention Models								  | Attention, Seq2Seq	                 |                                                        |
|10    |		 |Graph Neural Networks					     |10. Graph Neural Networks		                      | GCN, MPNN							 |  HW4 Due                                                                  |
|11    |		 |Memory network                             |11. Memory Networks                                 | Memory networks                      |                                                                            |
|12    |		 |Deep generative models					 |12. Deep generative models                          | GAN, VAE                             |  HW5 Due                                                                  |
|13    |		 |Project activities or guest lecture		 |                                                    |                                      |                                                                            |
|14    | 		 |Project activities or guest lecture		 |                                                    |                                      |                                                                            |
|15    |		 |Final project presentation				 |                                                    |                                      |                                                                            |
|16    |		 |Final project presentation				 |                                                    |                                      |                                                                            |

